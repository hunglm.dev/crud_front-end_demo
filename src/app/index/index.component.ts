import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../services/movie.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})

export class IndexComponent implements OnInit {
  movies: [];
  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit(): void {
    this.loadAllMovie();
  }

  loadAllMovie(): void {
    this.movieService.getAll().subscribe(
      data => {
        this.movies = data.data;
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

}
