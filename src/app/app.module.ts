import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {MovieItemComponent} from './movie/movie-item/movie-item.component';
import {AppRoutingModule} from './app.-routing.module';
import {HeaderComponent} from './fragsment/header/header.component';
import {FooterComponent} from './fragsment/footer/footer.component';
import {MovieService} from '../services/movie.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    MovieItemComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,

  ],
  providers: [MovieService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
