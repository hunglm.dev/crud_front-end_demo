import {Injectable} from '@angular/core';
import {environment} from '../environments/environment.prod';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) {}

  getAll(): Observable<any> {
    const endPoint = `${this.apiUrl}/getall`;
    return this.http.get(endPoint);
  }
}
